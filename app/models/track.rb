class Track < ActiveRecord::Base

  # Feed is updated daily, don't add duplicates for a single day
  # This will make sure the date, track and artist are unique
  # (A song won't show up twice in one day)
  validates_uniqueness_of :date, :scope => [:track_name, :artist_name]
  validates :date, :track_name, :artist_name, :album_name, :num_streams, presence: true

  def self.save_data_from_spotify_chart_api
    response = HTTParty.get('http://charts.spotify.com/api/tracks/most_streamed/global/daily/latest')
      track_data = JSON.parse(response.body)["tracks"]
	  tracks = track_data.map do |line|
	    t = Track.new
	    t.date = line['date']
	    t.country = line['country']
	    t.track_url = line['track_url']
	    t.track_name = line['track_name']
	    t.artist_name = line['artist_name']
	    t.artist_url = line['artist_url']
	    t.album_name = line['album_name']
	    t.album_url = line['album_url']
	    t.artwork_url = line['artwork_url']
	    t.num_streams = line['num_streams']
	    t.window_type = line['window_type']
	    t.save
	    t
	  end
  end
end