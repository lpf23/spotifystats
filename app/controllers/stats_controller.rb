class StatsController < ApplicationController

  def tracks_by_day
    render json: Track.group(:track_name).group_by_day(:date, format: "%Y-%m-%d").maximum(:num_streams).chart_json
  end
end
