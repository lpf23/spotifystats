# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Track.destroy_all

Track.create( 
	date: "2014-08-22", 
	country: "global", 
	track_url: "https:\/\/play.spotify.com\/track\/6RtPijgfPKROxEzTHNRiDp", 
	track_name: "Rude", 
	artist_name: "MAGIC!", 
	artist_url: "https:\/\/play.spotify.com\/artist\/0DxeaLnv6SyYk2DOqkLO8c", 
	album_name: "Don\u0027t Kill the Magic", 
	album_url: "https:\/\/play.spotify.com\/album\/0RZ4Ct4vegYBmL9g88TBNi", 
	artwork_url: "http:\/\/o.scdn.co\/300\/c36fb878b1a2c0c258a7b2808c0b2ea635978d24", 
	num_streams: 1148469, 
	window_type: "daily")

Track.create(
	date: "2014-08-22", 
	country: "global", 
	track_url: "https:\/\/play.spotify.com\/track\/6zSIjdKCen3LlHfJNTsxpE", 
	track_name: "Chandelier", 
	artist_name: "Sia", 
	artist_url: "https:\/\/play.spotify.com\/artist\/5WUlDfRSoLAfcVSX1WnrxN", 
	album_name: "Chandelier", 
	album_url: "https:\/\/play.spotify.com\/album\/2RHjpxr0NUzs8OKqvVy987", 
	artwork_url: "http:\/\/o.scdn.co\/300\/d176a1c1ceb12f4fbef27ba95ff03df4b8890ce3", 
	num_streams: 941733, 
	window_type: "daily")

Track.create( 
	date: "2014-08-22", 
	country: "global", 
	track_url: "https:\/\/play.spotify.com\/track\/7IHOIqZUUInxjVkko181PB", 
	track_name: "Stay With Me", 
	artist_name: "Sam Smith", 
	artist_url: "https:\/\/play.spotify.com\/artist\/2wY79sveU1sp5g7SokKOiI", 
	album_name: "In The Lonely Hour", 
	album_url: "https:\/\/play.spotify.com\/album\/7p7RFI5jtwYDknwhnQgmlp", 
	artwork_url: "http:\/\/o.scdn.co\/300\/72d5bd1ecd38f261de6b7987283f35cd13d02fbd", 
	num_streams: 1123279, 
	window_type: "daily")

Track.create(
	date: "2014-08-22",
	country: "global",
	track_url: "https:\/\/play.spotify.com\/track\/7cvxrDu7VIhVSOFRNgPegv",
	track_name: "Break Free",
	artist_name: "Ariana Grande",
	artist_url: "https:\/\/play.spotify.com\/artist\/66CXWjxzNUsdJxJ2JdwvnR",
	album_name: "Break Free",
	album_url: "https:\/\/play.spotify.com\/album\/3WXY7HsgHfXopEwlooZg2u",
	artwork_url: "http:\/\/o.scdn.co\/300\/9b4b4ea70bc8eadaa5e9c189b86110f42775d125",
	num_streams: 1035054,
	window_type: "daily")

Track.create(
	date: "2014-08-22",
	country: "global",
	track_url: "https:\/\/play.spotify.com\/track\/5zlC5d5umTrbcX9sLVVxzh",
	track_name: "Prayer in C - Robin Schulz Radio Edit",
	artist_name: "Lilly Wood \u0026 The Prick",
	artist_url: "https:\/\/play.spotify.com\/artist\/50OApTJurDusIo9dGTqSU4",
	album_name: "Prayer In C",
	album_url: "https:\/\/play.spotify.com\/album\/2lsnwv4RgOPPwwWV57hTZ9",
	artwork_url: "http:\/\/o.scdn.co\/300\/0bc99ea137889c196e69bf93908271c87be96ec4",
	num_streams: 1108947,
	window_type: "daily")

Track.create(
	date: "2014-08-22",
	country: "global",
	track_url: "https:\/\/play.spotify.com\/track\/7b71WsDLb8gG0cSyDTFAEW",
	track_name: "Summer",
	artist_name: "Calvin Harris",
	artist_url: "https:\/\/play.spotify.com\/artist\/7CajNmpbOovFoOoasH2HaY",
	album_name: "Summer",
	album_url: "https:\/\/play.spotify.com\/album\/0IGsZsrvIe5AQKvMmVobYq",
	artwork_url: "http:\/\/o.scdn.co\/300\/ca67ba1425e4308104ecae9fc493f3ef96ceb72a",
	num_streams: 828845,
	window_type: "daily")

Track.create(
	date: "2014-08-22",
	country: "global",
	track_url: "https:\/\/play.spotify.com\/track\/0ifSeVGUr7py5GggttDhXw",
	track_name: "All About That Bass",
	artist_name: "Meghan Trainor",
	artist_url: "https:\/\/play.spotify.com\/artist\/6JL8zeS1NmiOftqZTRgdTz",
	album_name: "All About That Bass",
	album_url: "https:\/\/play.spotify.com\/album\/2Gf7TGoOt6FyRi7p32OzUM",
	artwork_url: "http:\/\/o.scdn.co\/300\/d7fe19b11c2814f98aa1449713cdc42668070a49",
	num_streams: 1055610,
	window_type: "daily")

Track.create( 
	date: "2014-08-23", 
	country: "global", 
	track_url: "https:\/\/play.spotify.com\/track\/6RtPijgfPKROxEzTHNRiDp", 
	track_name: "Rude", 
	artist_name: "MAGIC!", 
	artist_url: "https:\/\/play.spotify.com\/artist\/0DxeaLnv6SyYk2DOqkLO8c", 
	album_name: "Don\u0027t Kill the Magic", 
	album_url: "https:\/\/play.spotify.com\/album\/0RZ4Ct4vegYBmL9g88TBNi", 
	artwork_url: "http:\/\/o.scdn.co\/300\/c36fb878b1a2c0c258a7b2808c0b2ea635978d24", 
	num_streams: 1458469, 
	window_type: "daily")

Track.create(
	date: "2014-08-23", 
	country: "global", 
	track_url: "https:\/\/play.spotify.com\/track\/6zSIjdKCen3LlHfJNTsxpE", 
	track_name: "Chandelier", 
	artist_name: "Sia", 
	artist_url: "https:\/\/play.spotify.com\/artist\/5WUlDfRSoLAfcVSX1WnrxN", 
	album_name: "Chandelier", 
	album_url: "https:\/\/play.spotify.com\/album\/2RHjpxr0NUzs8OKqvVy987", 
	artwork_url: "http:\/\/o.scdn.co\/300\/d176a1c1ceb12f4fbef27ba95ff03df4b8890ce3", 
	num_streams: 1141833, 
	window_type: "daily")

Track.create( 
	date: "2014-08-23", 
	country: "global", 
	track_url: "https:\/\/play.spotify.com\/track\/7IHOIqZUUInxjVkko181PB", 
	track_name: "Stay With Me", 
	artist_name: "Sam Smith", 
	artist_url: "https:\/\/play.spotify.com\/artist\/2wY79sveU1sp5g7SokKOiI", 
	album_name: "In The Lonely Hour", 
	album_url: "https:\/\/play.spotify.com\/album\/7p7RFI5jtwYDknwhnQgmlp", 
	artwork_url: "http:\/\/o.scdn.co\/300\/72d5bd1ecd38f261de6b7987283f35cd13d02fbd", 
	num_streams: 1134279, 
	window_type: "daily")

Track.create(
	date: "2014-08-23",
	country: "global",
	track_url: "https:\/\/play.spotify.com\/track\/7cvxrDu7VIhVSOFRNgPegv",
	track_name: "Break Free",
	artist_name: "Ariana Grande",
	artist_url: "https:\/\/play.spotify.com\/artist\/66CXWjxzNUsdJxJ2JdwvnR",
	album_name: "Break Free",
	album_url: "https:\/\/play.spotify.com\/album\/3WXY7HsgHfXopEwlooZg2u",
	artwork_url: "http:\/\/o.scdn.co\/300\/9b4b4ea70bc8eadaa5e9c189b86110f42775d125",
	num_streams: 1036154,
	window_type: "daily")

Track.create(
	date: "2014-08-23",
	country: "global",
	track_url: "https:\/\/play.spotify.com\/track\/5zlC5d5umTrbcX9sLVVxzh",
	track_name: "Prayer in C - Robin Schulz Radio Edit",
	artist_name: "Lilly Wood \u0026 The Prick",
	artist_url: "https:\/\/play.spotify.com\/artist\/50OApTJurDusIo9dGTqSU4",
	album_name: "Prayer In C",
	album_url: "https:\/\/play.spotify.com\/album\/2lsnwv4RgOPPwwWV57hTZ9",
	artwork_url: "http:\/\/o.scdn.co\/300\/0bc99ea137889c196e69bf93908271c87be96ec4",
	num_streams: 970077,
	window_type: "daily")

Track.create(
	date: "2014-08-23",
	country: "global",
	track_url: "https:\/\/play.spotify.com\/track\/7b71WsDLb8gG0cSyDTFAEW",
	track_name: "Summer",
	artist_name: "Calvin Harris",
	artist_url: "https:\/\/play.spotify.com\/artist\/7CajNmpbOovFoOoasH2HaY",
	album_name: "Summer",
	album_url: "https:\/\/play.spotify.com\/album\/0IGsZsrvIe5AQKvMmVobYq",
	artwork_url: "http:\/\/o.scdn.co\/300\/ca67ba1425e4308104ecae9fc493f3ef96ceb72a",
	num_streams: 949945,
	window_type: "daily")

Track.create(
	date: "2014-08-23",
	country: "global",
	track_url: "https:\/\/play.spotify.com\/track\/0ifSeVGUr7py5GggttDhXw",
	track_name: "All About That Bass",
	artist_name: "Meghan Trainor",
	artist_url: "https:\/\/play.spotify.com\/artist\/6JL8zeS1NmiOftqZTRgdTz",
	album_name: "All About That Bass",
	album_url: "https:\/\/play.spotify.com\/album\/2Gf7TGoOt6FyRi7p32OzUM",
	artwork_url: "http:\/\/o.scdn.co\/300\/d7fe19b11c2814f98aa1449713cdc42668070a49",
	num_streams: 945610,
	window_type: "daily")