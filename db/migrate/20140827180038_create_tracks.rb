class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.datetime :date
      t.string :country
      t.string :track_url
      t.string :track_name
      t.string :artist_name
      t.string :artist_url
      t.string :album_name
      t.string :album_url
      t.string :artwork_url
      t.integer :num_streams
      t.string :window_type

      t.timestamps
    end
  end
end
